extern crate gio;
extern crate gtk;


// TODO update packages
use std::env::args;
// use std::error::Error;
// use std::fs::File;
// use std::io::prelude::*;
// use std::iter::IntoIterator;
// use std::path::Path;
use std::process::Command;

use gio::prelude::*;
use gtk::prelude::*;

use gtk::{ApplicationWindow, Builder, Button, Entry, TextBuffer, TextView};


fn read_path() {
	let output = if cfg!(target_os = "windows") {
		Command::new("cmd")
				.args(&["/C", "$PATH"])
				.output()
				.expect("failed to execute process")
	} else {
		Command::new("sh")
				.arg("-c")
				.arg("echo $PATH")
				.output()
				.expect("failed to execute process")
	};

	let result = String::from_utf8_lossy(&output.stdout);
	// path_to_vec(result.to_string())
}


fn write_path(new_path: String) {
	let command: &str = &format!("export PATH = {}", new_path);
	let output = if cfg!(target_os = "windows") {
		Command::new("cmd")
				.arg("/C")
				.output()
				.expect("failed to execute process")
	} else {
		Command::new("sh")
				.arg("-c")
				.arg(command)
				.output()
				.expect("failed to execute process")
	};
}

// TODO implement add path
fn add_path() {}

// FIXME path_to_vec should return a "list" of strings
/*
fn path_to_vec(path: String, v: &Vec) {
	let split = path.split(":");
	let vec: Vec<&str> = split.collect();
	*v = vec;
}
*/

// TODO implement vec_to_path
fn vec_to_path() {}

// TODO implement save_to_file
fn save_to_file() -> String { "hello".to_string() }

// TODO implement load_from_file
fn load_from_file() {}


// FIXME load_view depends on how read_path is implemented
/*
fn load_view(buffer: TextBuffer) {
	let mut paths : String = "".to_string();
	let mut iter = read_path().into_iter();
	for path in iter {
		paths = paths.to_string() + &path.to_string() + &"\n".to_string();
	}
	buffer.set_text(&paths);
}
*/

fn build_ui(application: &gtk::Application) {
	let glade_src = include_str!("main.glade");
	let builder = Builder::new_from_string(glade_src);

	let window: ApplicationWindow = builder.get_object("window_main")
			.expect("Couldn't get window");
	window.set_application(application);

	let view: TextView = builder.get_object("textview_path")
			.expect("Couldn't get textview");

	let buffer: TextBuffer = view.get_buffer().expect("Couldn't get buffer");
	// load_view(buffer);

	let entry: Entry = builder.get_object("entry_new_path")
			.expect("Couldn't get entry");

	let button_add_path: Button = builder.get_object("button_add_path")
			.expect("Couldn't get Button");
	button_add_path.connect_clicked(move |_| {
		println!("Clicked");
		read_path();
	});

	let button_save_to_file: Button = builder.get_object("button_save_to_file")
			.expect("Couldnt get button_save_to_file");
	button_save_to_file.connect_clicked(move |_| {
		println!("saved");
	});

	let button_load_from_file: Button = builder
			.get_object("button_load_from_file")
			.expect("Couldn't get button_load_from_file");
	button_load_from_file.connect_clicked(move |_| {
		println!("load");
	});
	
	window.show_all();
}


fn main() {
	let application = gtk::Application::new(
					"org.satelliticthoughts.patheditor", Default::default())
			.expect("Initialization failed.");

	application.connect_activate(|app| {
		build_ui(app);
	});

	application.run(&args().collect::<Vec<_>>());
}

// TODO unittests
#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_save_to_file() {
		assert_eq!(save_to_file(), "helo".to_string());
	}
}

// TODO replace code in comment.
/*
fn get_path() -> String {
	let path = Path::new("path.txt");
	let display = path.display();

	let mut file = match File::open(&path) {
		Err(why) => panic!("Couldn't open {}: {}",display, why.description()),
		Ok(file) => file,
	};

	let mut s = String::new();
	match file.read_to_string(&mut s) {
		Err(why) => panic!("Couldn't read {}: {}", display, why.description()),
		Ok(_) => println!("{} contains:\n{}", display, s),
	};
	s
}

fn update_path(new_s: String) {
	let path = Path::new("path.txt");
	let display = path.display();

	let mut file = match File::create(&path) {
		Err(why) => panic!("Couldn't create {}: {}", display, why.description()),
		Ok(file) => file,
	};

	let text: String = get_path().to_string();
	
	let text_split = text.split(":");
	let mut temp_text: String = "".to_string();
	for s in text_split {
		temp_text = temp_text.to_string() + &s.to_string() + &"\n".to_string();
	}
	temp_text = temp_text.to_string() + &new_s.to_string() + &"\n".to_string();
	
	match file.write_all(temp_text.as_bytes()) {
		Err(why) => panic!("Couldn't write {}: {}", display, why.description()),
		Ok(_) => println!("Successfully wrote to {}", display),
	};
}

fn main() {
	let s = get_path();
	
	if gtk::init().is_err() {
		println!("Failed to initialize GTK.");
		return;
	}
	
	let glade_src = include_str!("main.glade");
	let builder = gtk::Builder::new_from_string(glade_src);

	let window: gtk::Window = builder.get_object("winMain").unwrap();

	let textarea: gtk::TextView = builder.get_object("txtAreaPath").unwrap();
	textarea.get_buffer().expect("Error with textview").set_text(&s);

	let entry: gtk::Entry = builder.get_object("entryNewPath").unwrap();
	let entry_clone = entry.clone();

	let button: gtk::Button = builder.get_object("btnAddPath").unwrap();
	button.connect_clicked(move |_| {
		update_path(entry_clone.get_buffer().get_text());
	});

	window.show_all();

	gtk::main();
}
*/
